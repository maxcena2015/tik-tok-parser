async function autoScroll(page) {
    await page.evaluate(async () => {
        await new Promise((resolve, reject) => {
            var totalHeight = 0;
            var distance = 100;
            var timer = setInterval(() => {
                var scrollHeight = document.body.scrollHeight;
                window.scrollBy(0, distance);
                totalHeight += distance;

                if (totalHeight >= scrollHeight) {
                    clearInterval(timer);
                    resolve();
                }
            }, 100);
        });
    });
}

function delay(time) {
    return new Promise(function(resolve) {
        setTimeout(resolve, time)
    });
}

const puppeteer = require('puppeteer');

let total_subs_count = 0;
let total_likes_count = 0;
let total_comments_count = 0;
let total_reposts_count =0;


(async () => {

    let tt_url = 'https://www.tiktok.com/@poplavskiy.m?';

    let browser = await puppeteer.launch();
    let page = await browser.newPage();

    await page.goto(tt_url);

    await page.setViewport({
        width: 1200,
        height: 800
    });
    await autoScroll(page);


    await page.waitForSelector('strong.video-count');

    let data = await page.evaluate(() => {

        let subs_count = document.querySelector('strong[title="Подписчики"]').innerText;
        let last_subs_count_char = subs_count.slice(-1);
        if(last_subs_count_char == 'K') {
            subs_count = subs_count.substring(0, subs_count.length - 1);
            subs_count = subs_count * 1000;
        }

        const hrefs = Array.from(
            document.querySelectorAll('.jsx-3109748587'),
            element => element.href
        );

        hrefs.push(subs_count);

        return hrefs

    });

    total_subs_count = Number(data[data.length - 1]);

    console.log(total_subs_count);

    await browser.close();

    let last_char;
    let video_url;
    let browser_vid;
    let page_vid;
    let data_vid;

    browser_vid = await puppeteer.launch();
    page_vid = await browser_vid.newPage();

    for(let i=1; i<data.length-2; i++) {

        video_url = data[i];

        await page_vid.goto(video_url);

            data_vid = await page_vid.evaluate(() => {

                let likes = 0;
                let comments = 0;
                let reposts = 0;

                if(document.querySelector('.jsx-3523196239')) {

                    likes = document.querySelector('.jsx-3523196239').innerText;
                    last_char = likes.slice(-1);
                    if (last_char == 'K') {
                        likes = likes.substring(0, likes.length - 1);
                        likes = likes * 1000;
                    }
                    likes = Number(likes);

                    comments = document.querySelector('.jsx-3523196239[title="comment"]').innerText;
                    last_char = comments.slice(-1);
                    if (last_char == 'K') {
                        comments = comments.substring(0, comments.length - 1);
                        comments = comments * 1000;
                    }
                    comments = Number(comments);

                    reposts = document.querySelector('.jsx-3523196239[title="share"]').innerText;
                    last_char = reposts.slice(-1);
                    if (last_char == 'K') {
                        reposts = reposts.substring(0, comments.length - 1);
                        reposts = reposts * 1000;
                    }
                    reposts = Number(reposts);
                }

                if (likes == NaN) likes == 0;
                if (comments == NaN) comments == 0;
                if (reposts == NaN) reposts == 0;

                return {
                    likes,
                    comments,
                    reposts
                };

            });

            if(data_vid['likes']==0 && data_vid['comments']==0) {

                tt_url = 'https://www.tiktok.com/@poplavskiy.m?';

                browser = await puppeteer.launch();
                page = await browser.newPage();

                await page.goto(tt_url);

                await page.setViewport({
                    width: 1200,
                    height: 800
                });
                await autoScroll(page);


                await page.waitForSelector('strong.video-count');

                console.log(('a[href="' + video_url + '"]'));

                await page.click(('a[href="' + video_url + '"]'));

                await page.waitForSelector('.jsx-2549575510');

                data_vid = await page.evaluate(() => {

                    let likes = document.querySelector('.jsx-2549575510.like-text').innerText;
                    last_char = likes.slice(-1);
                    if (last_char == 'K') {
                        likes = likes.substring(0, likes.length - 1);
                        likes = likes * 1000;
                    }

                    let comments = document.querySelector('.jsx-2549575510.comment-text').innerText;
                    last_char = comments.slice(-1);
                    if (last_char == 'K') {
                        comments = comments.substring(0, comments.length - 1);
                        comments = comments * 1000;
                    }

                    let reposts = 0;

                    return {
                        likes,
                        comments,
                        reposts
                    }

                });

                await browser.close();

            }

        total_likes_count = Number(total_likes_count) + Number(data_vid['likes']);
        console.log("Likes: " + total_likes_count);

        total_comments_count = Number(total_comments_count) + Number(data_vid['comments']);
        console.log("comments: " + total_comments_count);

        total_reposts_count = Number(total_reposts_count) + Number(data_vid['reposts']);
        console.log("reposts: " + total_reposts_count);

        await delay(3000);

    }

    await browser_vid.close();

    console.log("Total likes count is: " + total_likes_count);
    console.log("Total comments count is: " + total_comments_count);
    console.log("Total reposts count is: " + total_reposts_count);

    // get the client
    const mysql = require('mysql2');

    // create the connection to database
        const connection = mysql.createConnection({
           host: 'localhost',
           user: 'root',
            database: 'tiktok-parser'
      });

     const sql1 = `create table if not exists parsing(
    id int primary key auto_increment,
     subs int not null,
     likes int not null,
     comments int not null,
     reposts int not null
    )`;

     connection.query(sql1, function(err, results) {
            if(err) console.log(err);
            else console.log("Table was created");
        });

     const values = [total_subs_count, total_likes_count, total_comments_count, total_reposts_count];

       const sql2 = `INSERT INTO parsing(subs, likes, comments, reposts) VALUES (?,?,?,?)`;

       connection.query(sql2, values, function(err, results) {
          if(err) console.log(err);
          console.log(results);
       });

      connection.end();


})();
